from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import requests

from helpers.srgssr_accesstoken import access_token

class ActionRtsActu(Action):

    def name(self) -> Text:
        return 'action_rts_actu'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        # Query RTS Actu
        # This will return a list of recent audios/videos linked to "glacier"
        # The response will be equivalent to what you get here:
        # https://www.rts.ch/play/recherche?query=glacier
        token = access_token()
        request = requests.get('https://api.srgssr.ch/v2/rtslab/search_catalog',
                               headers={'Authorization': 'Bearer {}'.format(token),
                                        'accept': 'application/json'}, params={'query': 'glaciers', 'sort_by': 'publicationDate'})
        request_json = request.json()

        first_element = request_json["hits"]["hits"][0]["_source"]

        link = "https://www.rts.ch/play/tv/-/video/-?urn={}".format(first_element["urn"])
        image = first_element["image_url"]
        title = first_element["media_title"]

        dispatcher.utter_message(text=f"Voilà un article dans l'actualité ! Le titre est le suivant : {title}")
        dispatcher.utter_message(image=f'{image}')

        if "lead" in first_element:
            description = first_element["lead"]
            dispatcher.utter_message(text=f'Et en bonus, un petit preview : \n {description}')

        msg = {"type": "link", "payload": {"src": link}, "name":"actu du moment"}

        dispatcher.utter_message(text=f"Hésite pas à faire un tour sur la page pour en savoir plus !",  attachment=msg)

        return []
