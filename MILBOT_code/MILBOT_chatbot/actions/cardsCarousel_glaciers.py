from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

import json
import random

class cardsCarousel_glaciers(Action):

    def name(self) -> Text:
        return 'action_cards_glaciers'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        ####INFO : iframe == 0, gif ==1

        typeReply = ""

        iFrameOrGif = random.randint(0,1)
        if iFrameOrGif == 0:
            typeReply = "iframe"
        else:
            typeReply = "gif"

        file = open("actions/evolutionGlaciers.json")
        d = json.load(file)

        choix = d[typeReply]

        elem = random.choice(list(choix.keys()))
        data = choix[elem]

        if iFrameOrGif == 0:
            dispatcher.utter_message(json_message=data)
        else:
            dispatcher.utter_message(image=data["image"])
            dispatcher.utter_message(text="Ici, tu peux voir un gif du " + data["name"] + " fait à partir de cartes provenant de Swisstopo!")

        #data = {
        #    "payload": 'cardsCarousel_iframe',
        #    "data":[
        #        {
        #            "link": "https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1920&layers_timestamp=19201231&E=2748387.10&N=1202914.19&zoom=7.518594761554023",
        #            "index": "#one!"
        #        },
        #        {
        #            "link": "https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1961&layers_timestamp=19611231&E=2748387.10&N=1202914.19&zoom=7.518594761554023",
        #            "index": "#two!"
        #        },
        #        {
        #            "link": "https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=1999&layers_timestamp=19991231&E=2748387.10&N=1202914.19&zoom=7.518594761554023",
        #            "index": "#three!"
        #        },
        #        {
        #            "link": "https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=2003&layers_timestamp=20031231&E=2748387.10&N=1202914.19&zoom=7.518594761554023",
        #            "index": "#four!"
        #        },
        #        {
        #            "link": "https://map.geo.admin.ch/embed.html?topic=swisstopo&lang=fr&bgLayer=ch.swisstopo.pixelkarte-farbe&catalogNodes=1392&layers=ch.swisstopo.zeitreihen&time=2018&layers_timestamp=20181231&E=2748387.10&N=1202914.19&zoom=7.518594761554023",
        #            "index": "#five!"
        #        }
        #    ]
        #}

        #dispatcher.utter_message(json_message=data)


        return []
