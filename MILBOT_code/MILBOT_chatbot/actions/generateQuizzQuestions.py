from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

from bs4 import BeautifulSoup
import requests

import json
import random
import json
import pandas as pd

class QuizzGenerator(Action):

    def name(self) -> Text:
        return 'action_quizz'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        slot_value = tracker.get_slot("difficulte")
        file = None
        fileB = open("actions/quizEasyJSON.json")
        file = open("actions/quizJSON.json")
        data = json.load(file)
        dataB = json.load(fileB)
        listQuest = []
        listQuestB = []
        listNbKey = []
        listAnswer = []
        listAnswerB = []
        dFrame = {}
        dFrameB = {}
        while len(listQuest) < 4:
            questNb = random.choice(list(data.keys()))
            questionA = "question" + questNb
            questionB = "questionFacile" + questNb
            if questionA not in listQuest:
                listQuest.append(questionA)
                listQuestB.append(questionB)
                listNbKey.append(questNb)
                listAnswer.append(data[questNb]["answer"])
                listAnswerB.append(dataB[questNb]["answer"])
        dFrame["Question"] = listQuest
        dFrame["IndexQuestion"] = listNbKey
        dFrame["Answer"] = listAnswer
        dataFrame = pd.DataFrame(data=dFrame)

        dFrameB["Question"] = listQuestB
        dFrameB["IndexQuestion"] = listNbKey
        dFrameB["Answer"] = listAnswerB
        dataFrameB = pd.DataFrame(data=dFrameB)

        dataFrameB.to_csv("actions/quizzUsedB.csv", index=False, encoding="ISO-8859-1")

        dataFrame.to_csv("actions/quizzUsed.csv", index=False, encoding="ISO-8859-1")

        dispatcher.utter_message(text="Je vais te poser 4 questions, j'espère que t'es prêt.e !")
        #dispatcher.utter_message(response=f"utter_ask_question0")
        #dispatcher.utter_message(response=f"utter_ask_question1")
        #dispatcher.utter_message(response=f"utter_ask_question2")
        #dispatcher.utter_message(response=f"utter_ask_question3")

        #for row in df.itertuples(index=False):
        #    dispatcher.utter_message(response= f"utter_ask_{row.Question}")

        return []
