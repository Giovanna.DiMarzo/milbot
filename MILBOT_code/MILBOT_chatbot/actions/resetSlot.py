from typing import Any, Text, Dict, List
from rasa_sdk.events import SlotSet
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
class ResetSlotPostcode(Action):
    """Sets slotvalue to None"""
    def name(self):
        return "action_reset_slot_difficulte"

    def run(self, dispatcher, tracker, domain):
        return [SlotSet("difficulte", None)]