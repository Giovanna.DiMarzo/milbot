from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher

class StartGreeting(Action):

    def name(self) -> Text:
        return 'action_bonjour'

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        buttons = [{ "title": "Actualités", "payload": "/actus" },{ "title": "Infos", "payload": "Infos" },{ "title": "Actions", "payload": "Actions" },{"title": "Carte sur l'évolution des glaciers", "payload": "/carousel"},{ "title": "Quiz", "payload": "Quiz" }]
        dispatcher.utter_message(text=f"Hoi, ici Climo pour te servir. Je suis un chatbot issu de la collaboration entre la RTS, IMI et l'UNIGE, spécialisé sur la fonte des glaciers 🏔️🔜⛰️ et le climat. Que veux-tu savoir ? " , buttons=buttons)
        return []