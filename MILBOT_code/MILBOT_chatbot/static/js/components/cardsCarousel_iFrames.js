/**
 * creates horizontally placed cards carousel
 * @param {Array} cardsData json array
 */
function createCardsCarousel_iframe(cardsData) {
    let cards = "";
    cardsData.map((card_item) => {
        const item = `<div class="carousel-item" href=${card_item.index}>
                            <div class="card">
                                <iframe class="embed-responsive-item" src=${card_item.link} width='100%' height='100%' frameborder='0' style='border:0'></iframe>
                                <div class="card-content">
                                    <p>I am a very simple card. index : ${card_item.index}</p>
                                </div>
                            </div>
                        </div>`;
        cards += item;
    });
    const cardContents = `<img class="botAvatar" src="./static/img/rts_avatar.png"><div class="botMsg" id="carouContainer"><div class="carousel">${cards}</div></div><div class="clearfix"></div>`;
    return cardContents;
}

/**
 * appends cards carousel on to the chat screen
 * @param {Array} cardsToAdd json array
 */
function showCardsCarousel_iframe(cardsToAdd) {
    const cards = createCardsCarousel_iframe(cardsToAdd);

    $(cards).appendTo(".chats").show();

    $(document).ready(function(){
        $('.carousel').carousel({
            indicators: true
        });

    });
/*
    if (cardsToAdd.length <= 2) {
        $(`.cards_scroller>div.carousel_cards:nth-of-type(2)`).fadeIn(3000);
    } else {
        for (let i = 0; i < cardsToAdd.length; i += 1) {
            $(`.cards_scroller>div.carousel_cards:nth-of-type(${i})`).fadeIn(3000);
        }
        $(".cards .arrow.prev").fadeIn("3000");
        $(".cards .arrow.next").fadeIn("3000");
    }*/

    scrollToBottomOfResults();

    /*const card = document.querySelector("#paginated_cards");
    const card_scroller = card.querySelector(".cards_scroller");
    const card_item_size = 225;*/

    /**
     * For paginated scrolling, simply scroll the card one item in the given
     * direction and let css scroll snaping handle the specific alignment.
     */
    /*function scrollToNextPage() {
        card_scroller.scrollBy(card_item_size, 0);
    }

    function scrollToPrevPage() {
        card_scroller.scrollBy(-card_item_size, 0);
    }

    card.querySelector(".arrow.next").addEventListener("click", scrollToNextPage);
    card.querySelector(".arrow.prev").addEventListener("click", scrollToPrevPage);*/
    $(".usrInput").focus();
}