var open = false;
/* module for importing other js files */
function include(file) {
  const script = document.createElement('script');
  script.src = file;
  script.type = 'text/javascript';
  script.defer = true;


  document.getElementsByTagName('head').item(0).appendChild(script);
}

/**
// Bot pop-up intro
document.addEventListener("DOMContentLoaded", () => {
  const elemsTap = document.querySelector(".tap-target");
  // eslint-disable-next-line no-undef
  const instancesTap = M.TapTarget.init(elemsTap, {});
  instancesTap.open();
  setTimeout(() => {
    instancesTap.close();
  }, 4000);
});
**/
/* import components */
include('./static/js/components/index.js');

window.addEventListener('load', () => {
  // initialization
  $(document).ready(() => {
    // Bot pop-up intro
    $("div").removeClass("tap-target-origin");

    // drop down menu for close, restart conversation & clear the chats.
    $(".dropdown-trigger").dropdown();

    // initiate the modal for displaying the charts,
    // if you dont have charts, then you comment the below line
    $(".modal").modal();


  });
  // Toggle the chatbot screen
  $("#profile_div").click(() => {
    $(".profile_div")[0].classList.toggle('click');
    /*var widget = document.getElementById("widget");
    var toggle = document.getElementById("closeImg");
    var bottom = toggle.style.height;
    console.log(bottom);
    widget.style.bottom = -bottom + 5 + "%" ;*/
    $(".widget").toggle();
    open = !open
    // enable this if u have configured the bot to start the conversation.
    if(open){

        showBotTyping();
        $("#userInput").prop('disabled', true);

        // if you want the bot to start the conversation
        customActionTrigger();
    } else{
        $(".chats").fadeOut("normal", () => {
            $(".chats").html("");
            $(".chats").fadeIn();
        });
    }

  });

  // clear function to clear the chat contents of the widget.
  $("#clear").click(() => {
    $(".chats").fadeOut("normal", () => {
      $(".chats").html("");
      $(".chats").fadeIn();
    });
  });

  // close function to close the dropdown.
  $("#close_menu").click(() => {
    $("dropdown1").remove();
  });
});
