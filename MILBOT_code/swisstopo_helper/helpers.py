from selenium import webdriver
import time
from PIL import Image
import os

def download_pictures(name, url, year):
    driver = webdriver.Safari()
    driver.set_window_size(1000, 1000)
    driver.get(url)

    time.sleep(3)

    driver.save_screenshot("./pictures/{}/{}.png".format(name, year))
    driver.quit()

def resize_image(name, year, left, top, right, bottom):
    image = Image.open("./pictures/{}/{}.png".format(name, year))
    cropped_image = image.crop((left, top, right, bottom))

    cropped_image.save("./pictures/{}/{}_cropped.png".format(name, year))

    os.remove("./pictures/{}/{}.png".format(name, year))